# video-summary-report
**case study- From IIA Internship**:  
train a custom object detection for persons in the video using yoloV4 and Generate the video summary report such as number of persons in the video, number of peoples in the current frame, What are their age and gender.



[Output-video-folder-link:](https://drive.google.com/drive/folders/1fEWFlZMEXIrV8Jt9zEJB4t9uyuAOvt2G)

[siamese network-trained model](https://drive.google.com/file/d/1dQYT1Fna8jUskhMBVr0xI8oJdsBYRPmh/view?usp=sharing)

[custom-yolov3-weights](https://drive.google.com/drive/folders/1jQC5l3P7W5n5C-H6WLN0gh2V3FukWmYw?usp=sharing)

[custom-Yolov4-weights-for-person-dog-cat](https://drive.google.com/drive/folders/1tsyc0gzRNB3D7dAt76TmHBJrsgEFC99i?usp=sharing)

